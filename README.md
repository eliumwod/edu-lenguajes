# README #

### Para qué es este repo? ###

Este repo plantea un ejercicio sencillo para quien se inicia en la programación con diferentes lenguajes y no tiene gran recorrido en esta materia. Como requisitos previos se asumen nociones básicas de programación.

### Lo que debe el estudiante de programación implementar ###

Implementa una aplicación que, dados dos números, genere los 5000 primeros múltiplos de ambos.

## Antes de ponerte a trabajar...

##### Haz un fork del repositorio original ####

Haz un fork del repositorio original y configúralo de forma privada (la actividad propuesta es individual o en grupo cerrado de alumnos según se indique ;)
Habilita las issues.

Invita al profesor con permiso de escritura (Write).

##### Clona el repositorio ####
```
git clone <tu fork>
```

##### Tu rama de trabajo ####

Crea tu propia rama de trabajo! Crea una nueva rama a partir de master que se llame como el nombre de tu usuario en el curso. Te recuerdo cómo:

```
git checkout -b <usuario>
```

La evolución de tu solución final (si no estás trabajando en equipo) deberá estar apuntada por esta rama. Puedes utilizar todas las ramas que quieras, pero **no trabajes en la master** y asegúrate, si tienes otras ramas que forman parte de tu solución, de combinarlas con tu rama con el nombre de tu usuario.

###Documenta tu trabajo

El repo debe contener una carpeta nombrada como `doc`. [Sigue las instrucciones](doc/README.md) de cómo documentar.


##Cuándo termines tu trabajo...##

##### Etiqueta tu versión ####

Cuando tengas un revisión de tu código que consideres estable, etiquétala de la forma que te indique el [mecanismo de versionado](doc/versionado.md). Modifica tambien el [changelog](doc/changelog.md) indicando las novedades de la versión.
Puedes hacer etiquetado de tu último commit de la siguiente manera:

```
# Si quieres hacer una etiqueta ligera (solo nombrar un commit
git tag <etiqueta>

# Si quieres hacer una etiqueta que contenga más información
git tag -a <etiqueta> -m 'El mensaje'
```

Si quieres poner una etiqueta a un commit anterior, pon su checksum al final de las instrucciones anteriores.

Recuerda enviar tus tags a tus repos remotos de la siguiente manera:

```
git push <remoto> <tag>
```

Consulta esta [fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado) para más detalles.

## Estrategia de ramificación

Rama					| Uso
------------ 			| -------------
`master`	 			| Evolución del enunciado del ejercicio
`remote\usuario` 		| Evolución de la solución de cada alumno
`solucion-java`			| Rama que representa una solución del ejercicio en java
`solucion-c++`			| Rama que representa una solución del ejercicio en java
`solucion-python`		| Rama que representa una solución del ejercicio en java
`solucion-php`			| Rama que representa una solución del ejercicio en java
`soluciones`			| Rama que integra todas las soluciones

### Changelog de enunciado:

Se irán etiquetando enunciados consolidados y entregados a alumnos:

Tag				| Descripción
------------ 	| -------------
`enum-v1`		| Enunciado inicial
`enum-v2`		| En este cambio se indica que se trabajará en una rama concreta del alumno

### Snapshot actual del enunciado:

```Shell
.
├── README.md
├── c++
│   ├── dist
│   └── src
│       └── Multiplos.c
├── doc
│   ├── README.md
│   ├── c++
│   │   └── Work.md
│   ├── java
│   │   └── Work.md
│   ├── php
│   │   └── Work.md
│   └── python
│       └── Work.md
├── java
│   ├── README.md
│   ├── classes
│   ├── dist
│   └── src
│       └── Multiplos.java
├── php
│   └── index.php
└── python
    └── Multiplos.py
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

